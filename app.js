const port = process.env.APP_PORT || 3000;
const environment = process.env.NODE_ENV || 'development'

// inport packages
const express = require('express');
const app = express();
const server = require('http').createServer(app);
const shortid = require('shortid');
const morgan = require('morgan');
const cors = require('cors');
const socket_io = require('socket.io');

let io = socket_io(server);

// middlewares here
app.use(morgan('tiny'));
app.use(express.json());
app.use(cors());

// Endpoints
app.use('/api', require('./routes/api/cards').router);
app.use('/api', require('./routes/api/players').router);
app.use('/api', require('./routes/api/games').router);


Array.prototype.shuffle = function() {
	for (let i = this.length - 1; i > 0; i--) {
        const j = Math.floor(Math.random() * (i + 1));
        [this[i], this[j]] = [this[j], this[i]];
	}
	return this;
}

Array.prototype.popRandom = function() {
	const index = Math.floor(Math.random() * this.length);
	const element = this[index];
	this.splice(index, 1);
	return element;
}


// io.on('connection', (socket) => {
// 	socket.on('disconnect', () => {
// 		console.log("Socket disconnected");
// 	});

// 	socket.on('new-deck', (msg) => {
// 		Card.query().then(cards => {
// 			/* not the efficient way, but still working */
// 		});
// 	});
// });

server.listen(port, (err) => {
	if(!err)
		console.log(`Server running at http://localhost:${port}/`);
	else
		console.log('Error occurred while listening on port ' + port)
});


io.on('connection', socket => require('./routes/socket/games')(io, socket));

module.exports = {
	app, server	
}