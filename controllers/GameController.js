const shortid = require('shortid');
const { fn } = require('objection');

const { BaseController } = require('./BaseController');
const { CardController } = require('./CardController');

const { Game } = require('../models/Game');
const { Player } = require('../models/Player');
const { Card } = require('../models/Card');

class GameController extends BaseController{
    // api functions
    index(req, res){
        Game.query().withGraphFetched('[creator, whose_turn, players(orderByJoinedGameAt)]')
        .then(games => {
            if(games)
                res.json(games).status(200);
            else
                res.sendStatus(404);
        }).catch(err => super.errorHandler(err, res));
    }

    create(req, res){
        let tokens_to_win;
        switch (req.body.n_players) {
            case '2':
                tokens_to_win = 7;
                break;
            case '3':
                tokens_to_win = 5;
                break;
            case '4':
                tokens_to_win = 4;
                break;
            default:
                res.sendStatus(400);
                break;
        }
        if(typeof tokens_to_win !== 'undefined')
            Game.query().insertAndFetch({
                id: shortid.generate(),
                created_by_player_id: req.body.created_by_player_id,
                n_players: req.body.n_players,
                tokens_to_win: tokens_to_win,
                state: 'waiting',
            })
            .then(game => {
                res.json(game).status(201);
            })
            .catch(err => super.errorHandler(err, res));
    }
    
    retrieve(req, res){
        Game.query().findById(req.params.id).withGraphFetched('[creator, whose_turn, players(orderByJoinedGameAt)]')
        .then(game => {
            if(game)
                res.json(game).status(200);
            else
                res.sendStatus(404); /* NotFoundError should be thrown here */
        }).catch(err => super.errorHandler(err, res));
    }

    update(req, res){
        Game.query().updateAndFetchById(req.params.id, req.body).then(game => {
            if(game)
                res.json(game).status(200);
            else
                res.sendStatus(404); /* NotFoundError should be thrown here */
        }).catch(err => super.errorHandler(err, res));
    }

    delete(req, res){
        Game.query().deleteById(req.params.id)
        .then(() => res.sendStatus(200)).catch(err => super.errorHandler(err, res));
    }
};

module.exports =  {
    GameController: new GameController()
};