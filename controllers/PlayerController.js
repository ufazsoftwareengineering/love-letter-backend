const { BaseController } = require('./BaseController');
const { Player } = require('../models/Player');

class PlayerController extends BaseController{
    index(req, res){
        /* should be fetched with relations */
        Player.query().withGraphJoined('[card_on_hand, game, taken_card]')
        .then(players => {
            if(players)
                res.json(players).status(200);
            else
                res.sendStatus(404);
        }).catch(err => super.errorHandler(err, res));
    }

    create(req, res){
        // input have to be validated
        Player.query().insertAndFetch({
            nickname: req.body.nickname,
        }).withGraphFetched('[card_on_hand, game, taken_card]')
        .then(player => res.json(player).status(201))
        .catch(err => super.errorHandler(err, res));
    }
    
    retrieve(req, res){
        Player.query().findById(req.params.id)
        .withGraphJoined('[card_on_hand, game, taken_card]')
        .then(player => {
            if(player)
                res.json(player).status(200);
            else
                res.sendStatus(404); /* NotFoundError should be thrown here */
        })
        .catch(err => super.errorHandler(err, res));
    }

    update(req, res){
        Player.query().updateAndFetchById(req.params.id, req.body)
        .withGraphFetched('[card_on_hand, game, taken_card]')
        .then(player => {
            if(player)
                res.json(player).status(200);
            else
                res.sendStatus(404); /* NotFoundError should be thrown here */
        })
        .catch(err => super.errorHandler(err, res));
    }

    delete(req, res){
        Player.query().deleteById(req.params.id)
        .then(() => res.sendStatus(200)).catch(err => super.errorHandler(err, res));
    }
};

module.exports =  {
    PlayerController: new PlayerController()
};