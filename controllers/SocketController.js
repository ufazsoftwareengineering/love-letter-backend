const { fn } = require('objection');

const { BaseController } = require('./BaseController');

const { CardController } = require('./CardController');

const { Game } = require('../models/Game');
const { Player } = require('../models/Player');
const { Card } = require('../models/Card');


class SocketController extends BaseController{
    constructor(io, socket){
        super();
        this.io = io;
        this.socket = socket;   
    }

    // socket functions
    async joinGame({player, game}){
        try {
            game = await Game.query().findById(game.id).withGraphFetched('players(orderByJoinedGameAt)');
            player = await Player.query().findById(player.id).withGraphFetched('[card_on_hand, game, taken_card]');;
            if(!player)
                throw {
                    event: 'join-game',
                    msg: 'Player not found!' 
                };
            if(!game)
                throw {
                    event: 'join-game',
                    msg: 'Game not found!'
                };
            if(player.game_id == game.id)
                throw {
                    event: 'join-game',
                    msg: 'Player is already in this game!'
                };
            if(player.game_id)
                throw {
                    event: 'join-game',
                    msg: 'Player is already in another game!'
                };
            if(game.state !== 'waiting')
                throw {
                    event: 'join-game',
                    msg: `Game is already ${game.state}!`
                };
            if(game.players.length == game.n_players)
                throw {
                    event: 'join-game',
                    msg: 'Game is full!'
                };
            player = await Player.query().updateAndFetchById(player.id, {
                game_id: game.id,
                joined_game_at: fn.now(), 
                tokens_of_af: 0, 
                is_eliminated: false,
                is_protected: false,
                discarded_cards: JSON.stringify([])
            }).withGraphFetched('[card_on_hand, game, taken_card]');
            game = await Game.query().findById(game.id).withGraphFetched('players(orderByJoinedGameAt)');

            // socket events 
            this.socket.emit('player', player);
            this.socket.emit('game', game);
            this.socket.join(game.id);
            this.socket.to(game.id).emit('player-joined', player); // send to all in room EXcluding himself

            if(game.players.length == game.n_players){
                await this.startRound({game, whose_turn: game.players[0]});
            }
        } catch (err) {
			console.log('[ERROR] ' + JSON.stringify(err));	
			this.socket.emit('err', err);
        }
            
    }

    async startRound({game, whose_turn}){
        try {
            let deck = await CardController.generateDeck()
            deck.popRandom(); // discard a card
            let round = (game.curr_round || 0) + 1;
            game = await Game.query().updateAndFetchById(game.id, { 
                state: 'active', 
                curr_round: round,
                whose_turn_player_id: whose_turn.id, 
                deck: JSON.stringify(deck.map(card => card.id))
            }).withGraphFetched('[whose_turn, players(orderByJoinedGameAt)]');

            this.io.to(game.id).emit('round-started', game);
            await this.distributeCards({game});
            await this.drawCard({player: game.whose_turn});
            return {game}    
        } catch (err) {
			console.log('[ERROR] ' + JSON.stringify(err));	
			this.socket.emit('err', err);
        }
    }

    async distributeCards({game}){
        try {
            game = await Game.query().findById(game.id).withGraphFetched('[whose_turn, players(orderByJoinedGameAt)]');
            game.deck = JSON.parse(game.deck);
            await Promise.all(game.players.map(async participant => 
                Player.query().findById(participant.id).update({
                    on_hand_card_id: game.deck.popRandom(),
                    taken_card_id: null,
                    is_eliminated: false,
                    is_protected: false,
                    discarded_cards: JSON.stringify([])
                })
            ));
            game = await Game.query().updateAndFetchById(game.id, {
                    deck: JSON.stringify(game.deck)
                }).withGraphFetched('[whose_turn, players(orderByJoinedGameAt)]');

            this.io.to(game.id).emit('cards-distributed', game);
            return {game};
        } catch (err) {
			console.log('[ERROR] ' + JSON.stringify(err));	
			this.socket.emit('err', err);
        }
    }

    async drawCard({player}){
        try {
            player = await Player.query().findById(player.id).withGraphFetched('[card_on_hand, game, taken_card]');
            let game = await Game.query().findById(player.game_id);
            game.deck = JSON.parse(game.deck);
            if(player.on_hand_card_id)
                player = await Player.query().updateAndFetchById(player.id,   {
                    taken_card_id: game.deck.popRandom()
                }).withGraphFetched('[card_on_hand, game, taken_card]');
            else
                player = await Player.query().updateAndFetchById(player.id,   {
                    on_hand_card_id: game.deck.popRandom()
                }).withGraphFetched('[card_on_hand, game, taken_card]');
            player.game = await Game.query().updateAndFetchById(player.game_id, {
                deck: JSON.stringify(game.deck)
            });

            this.io.to(player.game_id).emit('card-drawn', player);
            return {player};
        }  catch (err) {
			console.log('[ERROR] ' + JSON.stringify(err));	
			this.socket.emit('err', err);
        }
    }

    async leaveGame({player}){
        try {
            player = await Player.query().findById(player.id).withGraphFetched('[card_on_hand, game, taken_card]');
            let game = await Game.query().findById(player.game_id).withGraphFetched('[whose_turn, players(orderByJoinedGameAt)]');
            if(!player)
                throw {
                    event: 'leave-game',
                    msg: 'Player not found!'
                };
            if(!game)
                throw {
                    event: 'join-game',
                    msg: 'Game not found!'
                };
            if(!player.game_id)
                throw {
                    event: 'leave-game',
                    msg: 'Player is not in a game!'
                };
            player = await Player.query().updateAndFetchById(player.id, {
                game_id: null, 
                tokens_of_af: null,
                on_hand_card_id: null,
                taken_card_id: null,
                is_eliminated: null, 
                is_protected: null,
                discarded_cards: null
            }).withGraphFetched('[card_on_hand, game, taken_card]');

            // socket events
            this.socket.emit('player', player);
            this.socket.leave(player.game_id);
            this.socket.to(game.id).emit('player-left', player); // send to all in room EXcluding himself  
        } catch (err) {
			console.log('[ERROR] ' + JSON.stringify(err));	
			this.socket.emit('err', err);
        }
        
    }

    async eliminatePlayer({player}){
        try {
            player = await Player.query().findById(player.id).withGraphFetched('[card_on_hand, game, taken_card]');
            let game = await Game.query().findById(player.game_id).withGraphFetched('[whose_turn, players(orderByJoinedGameAt)]');

            // add moved card to player's discarded cards
            player.discarded_cards = JSON.parse(player.discarded_cards);
            player.discarded_cards.push(player.on_hand_card_id);

            player = await Player.query().updateAndFetchById(player.id, {
                discarded_cards: JSON.stringify(player.discarded_cards), 
                on_hand_card_id: null, 
                taken_card_id: null,
                is_eliminated: true
            }).withGraphFetched('[card_on_hand, game, taken_card]');

            this.io.to(player.game_id).emit('player-discarded', player); // send to all in room INcluding himself  
            this.io.to(player.game_id).emit('player-eliminated', player); // send to all in room INcluding himself  

            // if non eliminated players in game == 1 ---> player won
            let non_eliminated = game.players.filter((pl) => !pl.is_eliminated);
            if(non_eliminated.length == 1)
                this.roundOver({game, non_eliminated});

            return {player}
        } catch (err) {
			console.log('[ERROR] ' + JSON.stringify(err));	
			this.socket.emit('err', err);
        }
    }

    async discardCard({player}){
        try {
            player = await Player.query().findById(player.id).withGraphFetched('[card_on_hand, game, taken_card]');
            if(player.card_on_hand.title == 'Princess')
                return {...await this.eliminatePlayer({player}), game: player.game};

            player.discarded_cards = JSON.parse(player.discarded_cards);
            player.discarded_cards.push(player.on_hand_card_id);

            player = await Player.query().updateAndFetchById(player.id, {
                    on_hand_card_id: null,
                    taken_card_id: null,
                    discarded_cards: JSON.stringify(player.discarded_cards)
                }).withGraphFetched('[card_on_hand, game, taken_card]');
            
            this.io.to(player.game_id).emit('player-discarded', player);

            ({player} = await this.drawCard({player}))
            return {player, game: player.game};
        } catch (err) {
			console.log('[ERROR] ' + JSON.stringify(err));	
			this.socket.emit('err', err);
        }
    }

    async playCard({player, card, target}){
        try {
            card = await Card.query().findById(card.id);
            player = await Player.query().findById(player.id).withGraphJoined('[card_on_hand, game, taken_card]');
            let game = await Game.query().findById(player.game_id).withGraphFetched('players(orderByJoinedGameAt)');

            // *** validate ***
            if(!card) 
                throw { event: 'play-card', msg: 'Card not found!' };
            if(!player) 
                throw { event: 'play-card', msg: 'Player not found!' };
            if(!player.game || !game) 
                throw { event: 'play-card', msg: 'Player is not in a game!' };
            if(player.game.state !== 'active') 
                throw { event: 'play-card', msg: 'The game player in, is not in active state!' };
            if(player.game.whose_turn_player_id != player.id) 
                throw { event: 'play-card', msg: 'It\'s not turn of this player!' };
            if(player.on_hand_card_id != card.id && player.taken_card_id != card.id) 
                throw { event: 'play-card', msg: 'The card does not belong to this player!' };
            if(player.is_eliminated)
                throw { event: 'play-card', msg: 'Player is already eliminated from this round!' };
            if(['Guard', 'Priest', 'Baron', 'Prince', 'King'].includes(card.title)){
                if(!target || !target.player)
                    throw { event: 'play-card', msg: `Target player for ${card.title} is not specified` };
                if(!game.players.find(element => element.id == target.player.id))
                    throw { event: 'play-card', msg: 'Target player is not in this game!' };
                if(card.title == 'Guard')
                    if(!target.card)
                        throw { event: 'play-card', msg: `Target card for ${card.title} is not specified` };
                    else if(target.card.id < 1 || target.card.id > 8)
                        throw { event: 'play-card', msg: 'Target card id is wrong!' };
                    else if(target.card.id == 1)
                        throw { event: 'play-card', msg: 'Guard cannot be chosen as a target!' };
            }
            if(card.title == 'Prince' || card.title == 'King') // force countess
                if(player.taken_card_id == 7 || player.on_hand_card_id == 7) // countess
                    throw { event: 'play-card', msg: 'You have to play Countess!' };
            if(target){
                if(target.player)
                    target.player = await Player.query().findById(target.player.id).withGraphJoined('card_on_hand');
                if(target.card)
                    target.card = await Card.query().findById(target.card.id);

                if(target.player.is_eliminated)
                    throw { event: 'play-card', msg: `Target player is already eliminated!` };

                if(target.player.is_protected) 
                    throw { event: 'play-card', msg: 'Target player is protected by Handmaid!' };

                if(game.players.filter((pl) => !pl.is_eliminated && !pl.is_protected).length > 1){
                    if(target.player.id == player.id && card.title != 'Prince' && card.title != 'Handmaid')
                        throw { event: 'play-card', msg: `You cannot choose yourself as a target for ${card.title}!` };
                }

            }
            // *** validate end ***

            // add moved card to player's discarded cards
            player.discarded_cards = JSON.parse(player.discarded_cards);
            player.discarded_cards.push(card.id);

            if(card.id === player.on_hand_card_id)
                player.on_hand_card_id = player.taken_card_id;

            player.taken_card_id = null;

            player = await Player.query().updateAndFetchById(player.id, {
                taken_card_id: player.taken_card_id, 
                on_hand_card_id: player.on_hand_card_id, 
                discarded_cards: JSON.stringify(player.discarded_cards)
            }).withGraphFetched('[card_on_hand, game, taken_card]');
            console.log('player', player)

            this.socket.to(game.id).emit('card-played', {player, card, target});

            if(target && target.player && (target.player.id == player.id && card.title != 'Prince')){
                // don't perform
                this.io.to(player.game_id).emit('player-discarded', player); // send to all in room INcluding himself  
            }
            else
                ({player, game} = await this.performMove({player, game, card, target}));

            
            game.whose_turn_player_id = await this.getNextTurnId({game});

            if(!game.whose_turn_player_id)
                await this.roundOver({game, winner: [player]});
                // await this.roundOver({game, winner: game.players.filter((pl) => !pl.is_eliminated)}); // TODO: to be tested
            else{
                game = await Game.query().updateAndFetchById(game.id, game).withGraphFetched('[players(orderByJoinedGameAt), whose_turn]');

                if(game.deck != '[]')
                    await this.drawCard({player: game.whose_turn});
                else{
                    // sort desc by on_hand_card_id (=strength) among cards !!!! bad but short way 
                    // we are using coincidence of id=strength
                    game.players.sort((pl1, pl2) => 
                        pl2.on_hand_card_id - pl1.on_hand_card_id
                    );

                    if(game.players[0].on_hand_card_id == game.players[1].on_hand_card_id) // tie
                        await this.roundOver({game, winner: [game.players[0], game.players[1]]});
                    else
                        await this.roundOver({game, winner: [game.players[0]]});
                }
            }

        } catch (err) {
			console.log('[ERROR] ' + JSON.stringify(err));	
			this.socket.emit('err', err);
        }
    }
    

    async performMove({player, game, card, target}){
        try {
            player.is_protected = false;
            switch (card.title) {
                case 'Guard': // + 
                    if(target.player.on_hand_card_id == target.card.id){ // eliminate here
                        ({player: target.player} = await this.eliminatePlayer({player: target.player}))
                        this.io.to(game.id).emit('info', `${player.nickname} played guard and ELIMINATED ${target.player.nickname}`);
                        console.log(`${player.nickname} played guard and ELIMINATED ${target.player.nickname}`);
                    }
                    else{
                        this.io.to(game.id).emit('info', `${player.nickname} played guard to ${target.player.nickname} and missed`);
                        console.log(`${player.nickname} played guard to ${target.player.nickname} and missed`);
                    }
                    break;
                case 'Priest': // + (easy)
                    this.io.to(game.id).emit('info', `${player.nickname} played priest and looked to card of ${target.player.nickname}`/* which is ${target.player.card_on_hand.title}*/);
                    console.log(`${player.nickname} played priest and looked to card of ${target.player.nickname}` /* which is ${target.player.card_on_hand.title}*/);
                    break;
                case 'Baron': // +
                    if(target.player.on_hand_card_id > player.on_hand_card_id){ // coincidence of id = strength
                        ({player} = await this.eliminatePlayer({player}))
                        this.io.to(game.id).emit('info', `${player.nickname} played baron to ${target.player.nickname} and have been ELIMINATED.` /*He had ${player.card_on_hand.title}, against ${target.player.card_on_hand.title} */);
                        console.log(`${player.nickname} played baron to ${target.player.nickname} and have been ELIMINATED.`/* He had ${player.card_on_hand.title}, against ${target.player.card_on_hand.title} */);
                    }
                    else if(target.player.on_hand_card_id < player.on_hand_card_id){ 
                        ({player: target.player} = await this.eliminatePlayer({player: target.player}))
                        this.io.to(game.id).emit('info', `${player.nickname} played baron to ${target.player.nickname} and ELIMINATED him.`/* He had ${player.card_on_hand}, against ${target.player.card_on_hand.title} */);
                        console.log(`${player.nickname} played baron to ${target.player.nickname} and ELIMINATED him.` /* He had ${player.card_on_hand}, against ${target.player.card_on_hand.title}` */);
                    }
                    else { // baron tie
                        this.io.to(game.id).emit('info', `${player.nickname} played baron to ${target.player.nickname} and TIE happened.`/*  They had ${player.card_on_hand.title}` */);
                        console.log(`${player.nickname} played baron to ${target.player.nickname} and TIE happened.` /* They had ${player.card_on_hand.title}`*/);
                    }
                    
                    break;
                case 'Handmaid': // +
                    player.is_protected = true;
                    this.io.to(game.id).emit('info', `${player.nickname} played handmaid and protected him(her-)self`);
                    console.log(`${player.nickname} played handmaid and protected him(her-)self`);
                    break;
                case 'Prince': // +
                    ({player: target.player, game} = await this.discardCard({player: target.player}));
                    this.io.to(game.id).emit('info', `${player.nickname} played prince and made ${target.player.nickname} discard his card`);
                    console.log(`${player.nickname} played prince and made ${target.player.nickname} discard his card`);
                    break;
                case 'King': // +
                    [target.player.on_hand_card_id, player.on_hand_card_id] = [player.on_hand_card_id, target.player.on_hand_card_id]; // swap cards
                    this.io.to(game.id).emit('info', `${player.nickname} played king and exchanged his hands with ${target.player.nickname}`);
                    console.log(`${player.nickname} played king and exchanged his hands with ${target.player.nickname}`);
                    break;
                case 'Countess': // +
                    this.io.to(game.id).emit('info', `${player.nickname} played countess`);
                    console.log(`${player.nickname} played countess`);
                    break;
                case 'Princess': // + 
                    player = (await this.eliminatePlayer({player})).player;
                    this.io.to(game.id).emit('info', `${player.nickname} played princess and ELIMINATED`);
                    console.log(`${player.nickname} played princess and ELIMINATED`);
                    break;
            }
            player = await Player.query().updateAndFetchById(player.id, player).withGraphFetched('[card_on_hand, game, taken_card]');;
            if(target && target.player)
                target.player = await Player.query().updateAndFetchById(target.player.id, target.player).withGraphFetched('[card_on_hand, game, taken_card]');
            return {player, game};
        } catch (err) {
			console.log('[ERROR] ' + JSON.stringify(err));	
			this.socket.emit('err', err);
        }
    }

    /**
     * Get whose is the next turn.  
     * Returns 0 if no active players left in game.
     * 
     * @param {game}
     * @reutrn ?int
     */
    async getNextTurnId({game}){
        try {
            game = await Game.query().findById(game.id).withGraphFetched('players(orderByJoinedGameAt)');
            let nextIndex;
            game.players.forEach((pl, i) => {
                if(pl.id == game.whose_turn_player_id)
                    if(i == game.players.length - 1)
                        nextIndex = 0;
                    else 
                        nextIndex = i + 1;
            });

            while(game.players[nextIndex].is_eliminated){
                if(nextIndex == game.players.length - 1)
                    nextIndex = 0;
                else 
                    nextIndex++;

                if(game.players[nextIndex].id == game.whose_turn_player_id)
                    return false; // round over
            }

            return game.players[nextIndex].id;
        } catch (err) {
			console.log('[ERROR] ' + JSON.stringify(err));	
			this.socket.emit('err', err);
        }
    }

    /**
     * @param {game, winner} winner is array! 
     */
    async roundOver({game, winner}){
        try {
            // increment the tokens of af
            winner.forEach(async (pl, i, a) => {
                a[i] = await Player.query().findById(pl.id).increment('tokens_of_af', 1)
            })

            // inform front that round is over
            this.io.emit('round-over', winner);

            winner.forEach(async (pl, i, a) => {
                if(pl.tokens_of_af == game.tokens_to_win)
                    await this.gameOver();
            })

            // sleep for 2 seconds
            await new Promise(resolve => setTimeout(resolve, 2000));
            this.startRound({game, whose_turn: winner[0]})
            console.log('Round over func end')    
        } catch (err) {
			console.log('[ERROR] ' + JSON.stringify(err));	
			this.socket.emit('err', err);
        }
    }

    // NOT COMPLETED
    async gameOver({game, winner}){
        // this.io.emit('game-over', {game, winner});
        // game.players.forEach(pl => {
        //     await Player.query().updateAndFetchById(pl.id, {
        //         game_id: null, 
        //         joined_game_at: null, 
        //         tokens_of_af: null, 
        //         on_hand_card_id: null, 
        //         taken_card_id: null, 
        //         discarded_cards: null
        //     });
        // })
        // await Game.query().updateAndFetchById(game.id, {
        //     state: 'over', 
        //     curr_round: null,
        //     whose_turn_player_id: null, 
        //     deck: null
        // });
    }
}

module.exports =  {
    SocketController
};