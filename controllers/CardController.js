const { BaseController } = require('./BaseController');
const { Card } = require('../models/Card');


class CardController extends BaseController{
    // api functions
    index(req, res){
        Card.query().then(cards => {
            res.json(cards).status(200);
        }).catch(err => super.errorHandler(err, res));
    }
    
    retrieve(req, res){
        Card.query().findById(req.params.id).then(card => {
            if(!card)
                res.sendStatus(404); /* NotFoundError should be thrown here */
            else
                res.status(200).json(card);
        }).catch(err => super.errorHandler(err, res));
    }

    // -----------

    generateDeck(){
        return Card.query().then(cards => {
            let deck = [];
            cards.forEach(card => {
                deck.push (...new Array(card.in_deck).fill(card));
            });
            return deck.shuffle();
        });
    }
};

module.exports =  {
    CardController: new CardController()
};