const {
    ValidationError,
    NotFoundError,
    DBError,
    ConstraintViolationError,
    UniqueViolationError,
    NotNullViolationError,
    ForeignKeyViolationError,
    CheckViolationError,
    DataError
  } = require('objection');
  
class BaseController{

    errorHandler(err, res) {
        /* LOG ERRORS HERE BETTER */
        console.error(err.message);

        if (err instanceof ValidationError)
            res.sendStatus(400);
        else if (err instanceof NotFoundError)
            res.sendStatus(404);
        else if (err instanceof UniqueViolationError)
            res.sendStatus(409);
        else if (err instanceof NotNullViolationError)
            res.sendStatus(400);
        else if (err instanceof ForeignKeyViolationError)
            res.sendStatus(409);
        else if (err instanceof CheckViolationError)
            res.sendStatus(400);
        else if (err instanceof DataError)
            res.sendStatus(400);
        else if (err instanceof DBError)
            res.sendStatus(500);
        else 
            res.sendStatus(500);
    }
}

module.exports = {
    BaseController: BaseController
}