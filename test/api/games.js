const chai = require('chai');
const chaiHttp = require('chai-http');
const { server } = require('../../app');
const fs = require('fs');
const should = chai.should();

chai.use(chaiHttp);

describe('GAMES', () => {
    let game;
    it('POST 200 Create game testing', (done) => {
            chai.request(server)
                .post('/api/player')
                .send({
                    'nickname': 'test-creator'    
                })
                .end((err, res) => {
                    let game1 = {
                        "created_by_player_id": `${res.body.id}`,
                        "n_players": "3"
                    }
                    chai.request(server)
                        .post('/api/game')
                        .send(game1)
                        .end((err, res) => {
                            res.should.be.json;
                            res.should.have.status(200);
                            game = res.body;
                            done();
                        }); 
                })
    });
    it('GET 400 created_by_player_id and n_players not provided', done => {
        let player = null;
        chai.request(server)
            .post('/api/game')
            .send(player)
            .end((err, res) => {
                res.should.have.status(400);
                done();
            });
    });
    it('GET 200 List all of the games', done => {
        chai.request(server)
            .get('/api/games')
            .end((err, res) => {
                res.should.have.status(200);
                res.should.be.json;
                res.body.should.be.a('array');
                done();
            });
    });
    it(`GET 200 List a single game`, done => {
        chai.request(server)
            .get(`/api/game/`+game.id)
            .end((err, res) => {
                res.should.be.json;
                res.should.have.status(200);
                done();
            });
    });   
    it(`DELETE 200 Delete a single game`, done => {
        chai.request(server)
            .delete(`/api/game/1`)
            .end((err, res) => {
                if(err){
                    throw err;
                }
                res.should.have.status(200);
                done();
            });
    });     
    

});