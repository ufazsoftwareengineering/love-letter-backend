const chai = require('chai');
const chaiHttp = require('chai-http');
const { server } = require('../../app');
const fs = require('fs');
const should = chai.should();

chai.use(chaiHttp);

describe('PLAYERS', () => {
    let player;
    it('POST 200 Create player testing', (done) => {
            let player1 = {
                'nickname': 'any_nickname'
            }
            chai.request(server)
                .post('/api/player')
                .send(player1)
                .end((err, res) => {
                    res.should.be.json;
                    res.should.have.status(200);
                    player = res.body;
                    done();
                });
    });
    it('GET 400 Nickname not provided', done => {
        let player = null;
        chai.request(server)
            .post('/api/player')
            .send(player)
            .end((err, res) => {
                res.should.have.status(400);
                done();
            });
    });
    it('GET 200 List all of the players', done => {
        chai.request(server)
            .get('/api/players')
            .end((err, res) => {
                res.should.have.status(200);
                res.should.be.json;
                res.body.should.be.a('array');
                done();
            });
    });
    it(`GET 200 List a single player`, done => {
        chai.request(server)
            .get(`/api/player/`+player.id)
            .end((err, res) => {
                res.should.be.json;
                res.should.have.status(200);
                done();
            });
    });   
    it(`DELETE 200 Delete a single player`, done => {
        chai.request(server)
            .delete(`/api/player/1`)
            .end((err, res) => {
                if(err){
                    throw err;
                }
                res.should.have.status(200);
                done();
            });
    });     
    

});