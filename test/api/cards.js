const chai = require('chai');
const chaiHttp = require('chai-http');
const { server } = require('../../app');
const fs = require('fs');
const should = chai.should();

const CARDS = JSON.parse(fs.readFileSync(__dirname + '/../../database/seeds/cards.json'))

chai.use(chaiHttp);

describe('CARDS', () => {
    it('GET 200 List all of the cards', done => {
        chai.request(server)
            .get('/api/cards')
            .end((err, res) => {
                res.should.have.status(200);
                res.should.be.json;
                res.body.should.be.a('array');
                res.body.should.to.deep.equal(CARDS);
                done();
            });
    });
    for (let i = 1; i <= 8; i++) {
        it(`GET 200 List a single card`, done => {
            chai.request(server)
                .get(`/api/card/${i}`)
                .end((err, res) => {
                    res.should.be.json;
                    res.should.have.status(200);
                    res.body.should.to.deep.equal(CARDS[i - 1]);
                    done();
                });
        });        
    }
    it('GET 404 Non existing card', done => {
        chai.request(server)
            .get('/api/cards/9')
            .end((err, res) => {
                res.should.have.status(404);
                done();
            });
    });
});