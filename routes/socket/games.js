let {SocketController} = require('../../controllers/SocketController');

module.exports = (io, socket) => {
	console.log(`Socket ${socket.id} connected!`);
	let controller = new SocketController(io, socket);

	socket.on('err', err => console.error('[ERROR] ' + JSON.stringify(err)));

	socket.on('connect-to-room', (id) => {socket.join(id); socket.emit('connected')});

	socket.on('join-game', async ({player, game}) => await controller.joinGame({player, game}));

	socket.on('leave-game', async ({player}) => await controller.leaveGame({player}));

	socket.on('play-card', async ({player, card, target}) => await controller.playCard({player, card, target}));
	
	socket.on('disconnect', () => {
		console.log(`Socket ${socket.id} disconnected!`);
	});
}