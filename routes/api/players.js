const express = require('express');
const router = express.Router();

const {Player} = require('../../models/Player');
const {PlayerController} = require('../../controllers/PlayerController');

router.get('/players', PlayerController.index);
router.post('/player', PlayerController.create);
router.get('/player/:id', PlayerController.retrieve);
router.put('/player/:id', PlayerController.update);
router.delete('/player/:id', PlayerController.delete);

module.exports = {
    router: router
};