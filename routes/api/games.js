const express = require('express');
const router = express.Router();
const shortid = require('shortid');

const { GameController } = require('../../controllers/GameController');

router.get('/games', GameController.index);
router.post('/game', GameController.create);
router.get('/game/:id', GameController.retrieve);
router.put('/game/:id', GameController.update);
router.delete('/game/:id', GameController.delete);

module.exports = {
    router: router
};