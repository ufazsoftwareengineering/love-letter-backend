const express = require('express');
const router = express.Router();

const Card = require('../../models/Card');
const { CardController } = require('../../controllers/CardController');

// list all of the cards
router.get('/cards', CardController.index);
// retrieve a card with given id
router.get('/card/:id', CardController.retrieve);

/* temporary solution just for testing */
router.get('/deck', (req, res) => {
    Card.query().then(cards => {
        let deck = [];
        cards.forEach(card => {
            deck.push (...new Array(card.in_deck).fill(card));
        });
        res.json(deck.shuffle()).status(200);
    });
});

module.exports = {
    router: router
};
