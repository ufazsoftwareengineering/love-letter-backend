FROM node:10

# Create app directory
ARG APP_DIR=/usr/src/app
WORKDIR $APP_DIR

# Install app dependencies
# A wildcard is used to ensure both package.json AND package-lock.json are copied
# where available (npm@5+)
COPY package*.json ./

RUN npm install --log-level=verbose

# install knex globally for knex cli
RUN npm install -g knex mocha

# If you are building your code for production
# RUN npm ci --only=production

# Bundle app source
COPY . .