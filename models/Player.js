const { BaseModel } = require('./BaseModel');

class Player extends BaseModel {
	static get tableName() {
		return 'players';
	} 

	$beforeInsert() {
		this.created_at = new Date().toISOString().slice(0, 19).replace('T', ' ');;
	}

	static get relationMappings() {
		return {
			game: {
			  relation: BaseModel.Model.BelongsToOneRelation,
			  modelClass: 'Game',
			  join: {
				from: 'players.game_id',
				to: 'games.id'
			  }
			},
			card_on_hand: {
			  relation: BaseModel.Model.BelongsToOneRelation,
			  modelClass: 'Card',
			  join: {
				from: 'players.on_hand_card_id',
				to: 'cards.id'
			  }
			},
			taken_card: {
				relation: BaseModel.Model.BelongsToOneRelation,
				modelClass: 'Card',
				join: {
				  from: 'players.taken_card_id',
				  to: 'cards.id'
				}
			}
		};
	}

	static get modifiers() {
		return {
		  orderByJoinedGameAt(builder) {
			builder.orderBy('joined_game_at');
		  }
		};
	}
}

module.exports = {
	Player: Player
};