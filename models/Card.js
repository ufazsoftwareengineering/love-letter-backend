const { BaseModel } = require('./BaseModel');

class Card extends BaseModel {
	static get tableName() {
		return 'cards';
	} 
}

module.exports = {
	Card: Card
};