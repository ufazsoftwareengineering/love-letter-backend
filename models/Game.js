const { BaseModel, fn } = require('./BaseModel');

class Game extends BaseModel {
	static get tableName() {
		return 'games';
	} 

	$beforeInsert() {
		this.created_at = this.updated_at = fn.now();
    }
    
    $beforeUpdate() {
		this.updated_at = fn.now();
	}

	static get relationMappings() {

		return {
			players: {
				relation: BaseModel.Model.HasManyRelation,
				modelClass: 'Player',
				join: {
				  from: 'games.id',
				  to: 'players.game_id'
				}
			},
			creator: {
				relation: BaseModel.Model.BelongsToOneRelation,
				modelClass: 'Player',
				join: {
					from: 'games.created_by_player_id',
					to: 'players.id'
				}
			},
			whose_turn: {
				relation: BaseModel.Model.BelongsToOneRelation,
				modelClass: 'Player',
				join: {
					from: 'games.whose_turn_player_id',
					to: 'players.id'
				}
			}
		};
	};
}

module.exports = {
	Game: Game
};