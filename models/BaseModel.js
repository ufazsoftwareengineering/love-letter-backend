const { Model, fn } = require('objection');
const { DBErrors } = require('objection-db-errors');
const { knex } = require('../database/knex');

// Usually you want to map each model class's errors. Easiest way to do this
// is to create a common superclass for all your models.

class BaseModel extends DBErrors(Model){
    static get Model(){
      return Model;
    }
    static get modelPaths() {
      return [__dirname];
    }
};

Model.knex(knex);

module.exports = {
  BaseModel,
  fn
};