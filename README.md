# Love Letter Backend

### Install requirements:

* docker 

        apt update -y 
        apt install -y apt-transport-https ca-certificates curl software-properties-common
        curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -    
        add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu bionic stable"
        apt update -y
        apt-cache policy docker-ce
        apt install -y docker-ce

*  docker-compose

        curl -L https://github.com/docker/compose/releases/download/1.21.2/docker-compose-`uname -s`-`uname -m` -o /usr/local/bin/docker-compose
        chmod +x /usr/local/bin/docker-compose

* npm

        apt-get install -y npm

---

### Prepare environment variables:


1. create .env file in project directory
2. copy content of .env.example file into the .env file
3. adjust env variables


---

## Testing
### Build 
`docker-compose -f docker-compose-test.yml build`
### Run:
`docker-compose -f docker-compose-test.yml up  --abort-on-container-exit --exit-code-from love-letter-test-backend `

---

## Deployment
### Build:

`docker-compose build`

### Run:
`docker-compose up`

