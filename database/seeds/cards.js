const fs = require('fs');

exports.seed = function(knex) {
  // Deletes ALL existing entries
  return knex('cards').del()
    .then(function () {
      // Inserts seed entries
      return knex('cards').insert(
        JSON.parse(fs.readFileSync(__dirname + '/cards.json'))
      );
    });
};