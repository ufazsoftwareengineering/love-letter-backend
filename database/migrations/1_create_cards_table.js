

exports.up = function(knex) {
    return knex.schema.createTable('cards', t => {
        t.increments('id').unsigned();
        t.string('title').notNullable();
        t.integer('strength').unsigned().notNullable();
        t.integer('in_deck').unsigned().notNullable();
        // t.text('effects').notNullable();
        // t.string('card_url').notNullable();
    });
};

exports.down = function(knex) {
    return knex.schema.dropTable('cards');
};
