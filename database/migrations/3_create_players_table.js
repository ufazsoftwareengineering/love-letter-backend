
exports.up = function(knex) {
    return knex.schema.createTable('players', t => {
        t.increments('id').unsigned();
        t.string('nickname').notNullable();
        t.boolean('is_bot').notNullable().defaultTo('false');
        t.timestamp('created_at').defaultTo(knex.fn.now()).notNullable();

        /* player in game details */
        t.string('game_id').nullable();
        t.boolean('is_eliminated').nullable();
        t.boolean('is_protected').nullable();
        t.timestamp('joined_game_at').nullable();
        t.integer('tokens_of_af').unsigned().nullable();
        t.integer('on_hand_card_id').unsigned().nullable();
        t.integer('taken_card_id').unsigned().nullable();
        t.json('discarded_cards').nullable();

        /* relations */
        t.foreign('game_id').references('games.id')
            .onUpdate('SET NULL').onDelete('SET NULL');
        t.foreign('on_hand_card_id').references('cards.id')
            .onUpdate('SET NULL').onDelete('SET NULL');
        t.foreign('taken_card_id').references('cards.id')
            .onUpdate('SET NULL').onDelete('SET NULL');
    });
};

exports.down = function(knex) {
    return knex.schema.dropTable('players');
};
