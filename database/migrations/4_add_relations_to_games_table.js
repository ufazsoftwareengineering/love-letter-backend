
exports.up = function(knex) {
    return knex.schema.table('games', (t) => {
        t.foreign('created_by_player_id').references('players.id')
            .onUpdate('SET NULL').onDelete('SET NULL');
        t.foreign('whose_turn_player_id').references('players.id')
            .onUpdate('SET NULL').onDelete('SET NULL');
    });
};

exports.down = function(knex) {
    return knex.schema.table('games', (t) => {
        t.dropForeign('created_by_player_id');
        t.dropForeign('whose_turn_player_id');
    });
};
