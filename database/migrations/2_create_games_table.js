
exports.up = function(knex) {
    return knex.schema.createTable('games', t => {
        t.string('id').primary();
        t.integer('n_players').unsigned().notNullable();
        t.integer('tokens_to_win').unsigned().notNullable();
        t.integer('created_by_player_id').unsigned().nullable();
        t.timestamps();

        t.enu('state', ['waiting', 'active', 'dropped', 'over']).notNullable();
        t.integer('curr_round').unsigned().nullable();
        t.integer('whose_turn_player_id').unsigned().nullable();
        t.json('deck').nullable();
    });
};

exports.down = function(knex) {
    return knex.schema.dropTable('games');
};
